//
//  AppDelegate.swift
//  CarDealer
//
//  Created by MAPPS MAC on 10/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var storyboard:UIStoryboard!
   
    var customTabBarVC:SSTabbarVC!
    
    var isRSvp:Bool!
    var isDirectReport:Bool!
    var arrEvent = ["Dealership Event 1","Dealership Event 2","Dealership Event 3","Dealership Event 4"]
    var arrEventDt = ["10/11/15","TODAY","11/8/15","11/12/15"]
    
    var arrAttendee = ["34","50","56","40"]
    var arrRsvp = ["65","57","78","56"]
    
     var arrName = ["Emily Folk","John Smith","Aaron Sanchez","Lisa Harris"]
    var selectIndex: Int!
    var selectEventIndex: Int!
    var homenav: UINavigationController!
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        storyboard = UIStoryboard(name: "Main", bundle:nil)
        //self.setUpTabBar()
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        return true
    }
    func setUpTabBar(){
        self.customTabBarVC = SSTabbarVC(nibName: "SSTabbarVC", bundle: nil)
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var homeVc : HomeVC = self.storyboard.instantiateViewControllerWithIdentifier("HomeVC") as! HomeVC
         homenav = UINavigationController(rootViewController: homeVc)
        homenav.navigationBarHidden = true
        var scanVc : ScannerVC = self.storyboard.instantiateViewControllerWithIdentifier("ScannerVC") as! ScannerVC
        var scannav: UINavigationController = UINavigationController(rootViewController: scanVc)
        scannav.navigationBarHidden = true
         var reportVc : ReportingVC = self.storyboard.instantiateViewControllerWithIdentifier("ReportingVC") as! ReportingVC
         var settVc : SettingsVC = self.storyboard.instantiateViewControllerWithIdentifier("SettingsVC")as! SettingsVC
        
        var profileV : ProfileView = self.storyboard.instantiateViewControllerWithIdentifier("ProfileView")as! ProfileView
        self.customTabBarVC.viewControllers = [homenav, scannav, reportVc,settVc]
        self.window!.rootViewController = customTabBarVC
        
        var addStatusBar = UIView()
        addStatusBar.frame = CGRectMake(0, 0, (self.window?.rootViewController?.view.frame.size.width)!, 20);
        addStatusBar.backgroundColor = UIColor.blackColor()
        self.window?.rootViewController?.view .addSubview(addStatusBar)
        

    }
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

