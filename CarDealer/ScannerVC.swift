//
//  ScannerVC.swift
//  CarDealer
//
//  Created by Aditi on 12/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit
import AVFoundation
enum EventTypeS: Int {
    case action1 = 0
}
typealias EventCompletionHandlerS = (obj:AnyObject?,EventTypeS:Int ) -> Void

class ScannerVC: UIViewController,AVCaptureMetadataOutputObjectsDelegate {
    
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblGlobalTitle: UILabel!
    @IBAction func btnSearchAction(sender: UIButton) {
        self.btnSearch.hidden = true
        self.lblGlobalTitle.hidden = true
        self.viewSearch.hidden = false
        self.txtSearch .becomeFirstResponder()
    }
    
    @IBAction func btnCancelAction(sender: UIButton) {
        self.btnSearch.hidden = false
        self.lblGlobalTitle.hidden = false
        self.viewSearch.hidden = true
        self.txtSearch .resignFirstResponder()
        
    }
    @IBOutlet weak var viewSearch: UIView!

    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    

    @IBOutlet weak var QrCodeView: UIView!
    @IBOutlet weak var scanView: UIView!
     @IBOutlet weak var lblTitle: UILabel!
    
    // Added to support different barcodes
    let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]

    var eventCompletionHandlerS :EventCompletionHandlerS!
    func setEventOnCompletion(handler:EventCompletionHandlerS){
        eventCompletionHandlerS = handler
    }
    func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        self.txtSearch.resignFirstResponder()
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtSearch.autocorrectionType = .No
      // self.scanView.backgroundColor = UIColor .blackColor();
       // self.scanView.alpha = 0.8
        
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            
            // Detect all the supported bar code
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            print(self.QrCodeView.layer.frame)
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            
            self.QrCodeView.translatesAutoresizingMaskIntoConstraints = true;
            self.QrCodeView.frame = CGRectMake(20, 30, self.view.frame.size.width - 70, 199);
             print(self.QrCodeView.layer.frame)
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = self.QrCodeView.layer.frame
             print(self.QrCodeView.layer.frame)
            print(videoPreviewLayer?.frame)
            self.QrCodeView.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture
            captureSession?.startRunning()
            
            
//            // Initialize QR Code Frame to highlight the QR code
//            qrCodeFrameView = UIView()
//            
//            if let qrCodeFrameView = qrCodeFrameView {
//                qrCodeFrameView.layer.borderColor = UIColor.greenColor().CGColor
//                qrCodeFrameView.layer.borderWidth = 2
//                view.addSubview(qrCodeFrameView)
//                view.bringSubviewToFront(qrCodeFrameView)
//            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            self.QrCodeView?.frame = CGRectZero
           // messageLabel.text = "No barcode/QR code is detected"
            let alert = UIAlertView()
            alert.title = "QR Code Scanner"
            alert.message = "No barcode/QR code is detected"
            alert.addButtonWithTitle("OK")
            alert.show()
            //return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        // Here we use filter method to check if the type of metadataObj is supported
        // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
        // can be found in the array of supported bar codes.
        if supportedBarCodes.contains(metadataObj.type) {
            //        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj)
            self.QrCodeView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
               let alert = UIAlertView()
                alert.title = "QR Code Scanner"
                alert.message = metadataObj.stringValue
                alert.addButtonWithTitle("OK")
                alert.show()

            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
