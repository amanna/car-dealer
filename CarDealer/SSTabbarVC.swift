//
//  SSTabbarVC.swift
//  CarDealer
//
//  Created by MAPPS MAC on 22/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit

class SSTabbarVC: UITabBarController {
  var customTabContainer : UIView!
    
   var appDelegate : AppDelegate!
    @IBOutlet weak var btnHome: UIButton!
    
    @IBOutlet weak var btnScan: UIButton!
    
    @IBOutlet weak var btnReport: UIButton!
    
    @IBOutlet weak var btnSettings: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.existingTabBar()
        var nibObjects: [AnyObject] = NSBundle.mainBundle().loadNibNamed("SSTabbarVC", owner: self, options: nil)
        self.customTabContainer = nibObjects[0] as! UIView
        self.customTabContainer.frame = CGRectMake(0, self.view.frame.size.height - self.customTabContainer.frame.size.height, self.view.frame.size.width, self.customTabContainer.frame.size.height)
        self.view!.addSubview(self.customTabContainer)
        //self.selectedTab(self.customTabContainer.subviews[0])


        // Do any additional setup after loading the view.
    }
    func selectView(btnTag: Int) {
         self.selectedIndex = btnTag
        if(self.selectedIndex==0){
            self.btnHome.setBackgroundImage(UIImage(named:"home_hover.png"), forState:UIControlState.Normal)
            
            self.btnScan.setBackgroundImage(UIImage(named:"qr-code.png"), forState:UIControlState.Normal)
            self.btnReport.setBackgroundImage(UIImage(named:"reports.png"), forState:UIControlState.Normal)
            self.btnSettings.setBackgroundImage(UIImage(named:"settings.png"), forState:UIControlState.Normal)
            
        }else if(self.selectedIndex==1){
            self.btnHome.setBackgroundImage(UIImage(named:"home.png"), forState:UIControlState.Normal)
            self.btnScan.setBackgroundImage(UIImage(named:"qr_code_hover.png"), forState:UIControlState.Normal)
            self.btnReport.setBackgroundImage(UIImage(named:"reports.png"), forState:UIControlState.Normal)
            self.btnSettings.setBackgroundImage(UIImage(named:"settings.png"), forState:UIControlState.Normal)
        }else if(self.selectedIndex==2){
            
            self.btnHome.setBackgroundImage(UIImage(named:"home.png"), forState:UIControlState.Normal)
            self.btnScan.setBackgroundImage(UIImage(named:"qr-code.png"), forState:UIControlState.Normal)
            self.btnReport.setBackgroundImage(UIImage(named:"reports_hover.png"), forState:UIControlState.Normal)
            self.btnSettings.setBackgroundImage(UIImage(named:"settings.png"), forState:UIControlState.Normal)
        }else{
            self.btnHome.setBackgroundImage(UIImage(named:"home.png"), forState:UIControlState.Normal)
            self.btnScan.setBackgroundImage(UIImage(named:"qr-code.png"), forState:UIControlState.Normal)
            self.btnReport.setBackgroundImage(UIImage(named:"reports.png"), forState:UIControlState.Normal)
            self.btnSettings.setBackgroundImage(UIImage(named:"settings_hover.png"), forState:UIControlState.Normal)
        }

    }
    @IBAction func selectedTab(sender: AnyObject) {
        appDelegate.isDirectReport = true
        let tabBtn: UIButton = (sender as! UIButton)
        self.selectedIndex = tabBtn.tag
        
        if(self.selectedIndex==0){
            print(appDelegate.homenav.viewControllers.count)
            if(appDelegate.homenav.viewControllers.count > 1){
                appDelegate.homenav .popToRootViewControllerAnimated(true)
            }
            self.btnHome.setBackgroundImage(UIImage(named:"home_hover.png"), forState:UIControlState.Normal)
            
            self.btnScan.setBackgroundImage(UIImage(named:"qr-code.png"), forState:UIControlState.Normal)
            self.btnReport.setBackgroundImage(UIImage(named:"reports.png"), forState:UIControlState.Normal)
            self.btnSettings.setBackgroundImage(UIImage(named:"settings.png"), forState:UIControlState.Normal)
           
        }else if(self.selectedIndex==1){
            self.btnHome.setBackgroundImage(UIImage(named:"home.png"), forState:UIControlState.Normal)
            self.btnScan.setBackgroundImage(UIImage(named:"qr_code_hover.png"), forState:UIControlState.Normal)
            self.btnReport.setBackgroundImage(UIImage(named:"reports.png"), forState:UIControlState.Normal)
            self.btnSettings.setBackgroundImage(UIImage(named:"settings.png"), forState:UIControlState.Normal)
        }else if(self.selectedIndex==2){
            
            self.btnHome.setBackgroundImage(UIImage(named:"home.png"), forState:UIControlState.Normal)
            self.btnScan.setBackgroundImage(UIImage(named:"qr-code.png"), forState:UIControlState.Normal)
            self.btnReport.setBackgroundImage(UIImage(named:"reports_hover.png"), forState:UIControlState.Normal)
            self.btnSettings.setBackgroundImage(UIImage(named:"settings.png"), forState:UIControlState.Normal)
        }else{
            self.btnHome.setBackgroundImage(UIImage(named:"home.png"), forState:UIControlState.Normal)
            self.btnScan.setBackgroundImage(UIImage(named:"qr-code.png"), forState:UIControlState.Normal)
            self.btnReport.setBackgroundImage(UIImage(named:"reports.png"), forState:UIControlState.Normal)
            self.btnSettings.setBackgroundImage(UIImage(named:"settings_hover.png"), forState:UIControlState.Normal)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func existingTabBar(){
        for view: UIView in self.view.subviews {
            if (view .isKindOfClass(UITabBar)) {
                view.hidden = true
            }
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
