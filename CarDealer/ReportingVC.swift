//
//  ReportingVC.swift
//  CarDealer
//
//  Created by Aditi on 13/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit
enum EventTypeR: Int {
    case action1 = 0
}
typealias EventCompletionHandlerR = (obj:AnyObject?,EventTypeR:Int ) -> Void

class ReportingVC: UIViewController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblGlobalTitle: UILabel!
    @IBAction func btnSearchAction(sender: UIButton) {
        self.btnSearch.hidden = true
        self.lblGlobalTitle.hidden = true
        self.viewSearch.hidden = false
        self.txtSearch .becomeFirstResponder()
    }
    
    @IBAction func btnCancelAction(sender: UIButton) {
        self.btnSearch.hidden = false
        self.lblGlobalTitle.hidden = false
        self.viewSearch.hidden = true
        self.txtSearch .resignFirstResponder()
        
    }
    func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        self.txtSearch.resignFirstResponder()
        return true
    }
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewSchedule: UIView!
    
    @IBOutlet weak var lblEventTitle: UILabel!
    @IBOutlet weak var viewSales: UIView!
    
    @IBOutlet weak var titleView: UIView!
    
     var appDelegate:AppDelegate!
    var eventCompletionHandlerR :EventCompletionHandlerR!
    func setEventOnCompletion(handler:EventCompletionHandlerR){
        eventCompletionHandlerR = handler
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtSearch.autocorrectionType = .No
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.viewSchedule.backgroundColor = UIColor .blackColor();
        self.viewSchedule.alpha = 0.85
        self.viewSales.backgroundColor = UIColor .blackColor();
        self.viewSales.alpha = 0.85
        
        if(appDelegate.isDirectReport==true){
            self.titleView.hidden = true
            
        }else{
            self.titleView.hidden = false
            self.lblEventTitle.text = appDelegate.arrEvent[appDelegate.selectEventIndex]
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
