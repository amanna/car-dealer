//
//  EventDetailsVC.swift
//  CarDealer
//
//  Created by Aditi on 12/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit

class EventDetailsVC: UIViewController {
     @IBOutlet weak var lblTitle: UILabel!
     var appDelegate:AppDelegate!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblGlobalTitle: UILabel!
    @IBAction func btnSearchAction(sender: UIButton) {
        self.btnSearch.hidden = true
        self.lblGlobalTitle.hidden = true
        self.viewSearch.hidden = false
        self.txtSearch .becomeFirstResponder()
    }
    
    @IBAction func btnCancelAction(sender: UIButton) {
        self.btnSearch.hidden = false
        self.lblGlobalTitle.hidden = false
        self.viewSearch.hidden = true
         self.txtSearch .resignFirstResponder()
        
    }
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var lblAttendee: UILabel!
    @IBOutlet weak var lblRsVp: UILabel!
    @IBOutlet weak var viewRsVp: UIView!
    
    @IBOutlet weak var viewAttendee: UIView!
    @IBAction func btnScanAction(sender: UIButton) {
       appDelegate.customTabBarVC.selectView(1)
    }
    
    @IBAction func btnBackAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func btnViewReportAction(sender: UIButton) {
        appDelegate.isDirectReport = false
        appDelegate.customTabBarVC.selectView(2)
    }
    
    @IBAction func btnRSVPAction(sender: UIButton) {
         appDelegate.isRSvp=true
        self.performSegueWithIdentifier("segueRsvp", sender: self)
    }
    
    @IBAction func btnAttendeeAction(sender: UIButton) {
        appDelegate.isRSvp=false
        self.performSegueWithIdentifier("segueRsvp", sender: self)
    }
    func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        self.txtSearch.resignFirstResponder()
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         self.txtSearch.autocorrectionType = .No
         appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.lblTitle.text = appDelegate.arrEvent[appDelegate.selectEventIndex]
        
        self.lblRsVp.text = appDelegate.arrRsvp[appDelegate.selectEventIndex]
        self.lblAttendee.text = appDelegate.arrAttendee[appDelegate.selectEventIndex]
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("attendeeAction"))
        self.viewAttendee.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: Selector("rsvpAction"))
        self.viewRsVp.addGestureRecognizer(tap1)
        // Do any additional setup after loading the view.
    }
    func attendeeAction(){
        appDelegate.isRSvp=false
         self.performSegueWithIdentifier("segueRsvp", sender: self)
    }
    func rsvpAction(){
        appDelegate.isRSvp=true
         self.performSegueWithIdentifier("segueRsvp", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
