//
//  AttendeeVC.swift
//  CarDealer
//
//  Created by Aditi on 12/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit

class AttendeeVC: UIViewController {
    
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblGlobalTitle: UILabel!
    @IBAction func btnSearchAction(sender: UIButton) {
        self.btnSearch.hidden = true
        self.lblGlobalTitle.hidden = true
        self.viewSearch.hidden = false
        self.txtSearch .becomeFirstResponder()
    }
    
    @IBAction func btnCancelAction(sender: UIButton) {
        self.btnSearch.hidden = false
        self.lblGlobalTitle.hidden = false
        self.viewSearch.hidden = true
        self.txtSearch .resignFirstResponder()
        
    }
    @IBOutlet weak var viewSearch: UIView!

    
    
    var arrName = ["Emily Folk","John Smith","Aaron Sanchez","Lisa Harris"]
    var arrCount = ["(2)","(1)","(5)","(1)"]
    var arrTime = ["10:30am","12:00 pm","1:30 pm","3:00pm"]
    var arrSection = ["October 16th","October 17th"]
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var myView: UIView!
    @IBAction func btnBackAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    internal  var isRSVP : Bool!
    @IBOutlet weak var viewRsvp: UIView!
    
    @IBOutlet weak var lblRsvpSep: UILabel!
    @IBOutlet weak var viewAttendee: UIView!
    
    @IBOutlet weak var lblAttendeeSep: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var searchRsVp: UIView!
    
    var appDelegate:AppDelegate!
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        self.txtSearch.resignFirstResponder()
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtSearch.autocorrectionType = .No
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let nib = UINib(nibName: "RSVPCell", bundle: nil)
        self.tblView.registerNib(nib, forCellReuseIdentifier: "CellR")
        
        let nib1 = UINib(nibName: "AttendeeCell", bundle: nil)
        self.tblView.registerNib(nib1, forCellReuseIdentifier: "CellA")

         self.tblView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.searchRsVp.backgroundColor = UIColor .blackColor();
        self.searchRsVp.alpha = 0.5
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("rsvpAction"))
        self.viewRsvp.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: Selector("attendeeAction"))
        self.viewAttendee.addGestureRecognizer(tap1)
        if(appDelegate.isRSvp==true){
            self.isRSVP=true
        }else{
            self.isRSVP=false
        }
        
        if(self.isRSVP==true){
            self.lblRsvpSep.hidden = false
            self.lblAttendeeSep.hidden = true
            self.viewRsvp.backgroundColor = UIColor.blackColor()
            self.viewAttendee.backgroundColor = UIColor(red: 54/255, green: 61/255, blue:69/255, alpha: 0.5)

        }else{
            self.lblRsvpSep.hidden = true
            self.lblAttendeeSep.hidden = false
            self.viewRsvp.backgroundColor = UIColor(red: 54/255, green: 61/255, blue:69/255, alpha: 0.5)
            self.viewAttendee.backgroundColor = UIColor.blackColor()

        }
        
      
        // Do any additional setup after loading the view.
    }
    
    func rsvpAction(){
        self.isRSVP = true
        self.lblRsvpSep.hidden = false
        self.lblAttendeeSep.hidden = true
        self.viewRsvp.backgroundColor = UIColor.blackColor()
        self.viewAttendee.backgroundColor = UIColor(red: 54/255, green: 61/255, blue:69/255, alpha: 0.5)
        self.tblView .reloadData()
    }
    
    func attendeeAction(){
        self.isRSVP = false
        self.lblRsvpSep.hidden = true
        self.lblAttendeeSep.hidden = false
        self.viewRsvp.backgroundColor = UIColor(red: 54/255, green: 61/255, blue:69/255, alpha: 0.5)
        self.viewAttendee.backgroundColor = UIColor.blackColor()
        self.tblView .reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section==0){
            return 1
        }else{
            return 4
        }
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 50)) //set these values as necessary
        returnedView.backgroundColor = UIColor(red: 47/255, green: 153/255, blue: 56/255, alpha: 1.0)
        
        let label = UILabel(frame: CGRectMake(0, 0, tableView.frame.size.width, returnedView.frame.size.height))
        label.text = self.arrSection[section]
        label.textColor = UIColor.whiteColor()
        label.font = UIFont(name: "OpenSans-Semibold", size: 25.0)
        label.textAlignment = NSTextAlignment.Center;
        returnedView.addSubview(label)
        
        return returnedView
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 56.0;//Choose your custom row height
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(self.isRSVP==true){
        let cell:RSVPCell = tableView.dequeueReusableCellWithIdentifier("CellR") as! RSVPCell
        cell.lblName.text = self.arrName[indexPath.row];
        cell.lblCount.text = self.arrCount[indexPath.row];
        cell.lblTime.text = self.arrTime[indexPath.row];
        let tot = tableView.numberOfRowsInSection(indexPath.section)
        if(indexPath.row == tot - 1){
            cell.lblSep.hidden = true
        }else{
           cell.lblSep.hidden = false
        }
            
            cell.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor .clearColor();
            backgroundView.alpha = 0.5
            cell.selectedBackgroundView = backgroundView

        return cell
        }else{
            let cell:AttendeeCell = tableView.dequeueReusableCellWithIdentifier("CellA") as! AttendeeCell
            cell.lblName.text = self.arrName[indexPath.row];
            cell.lblCount.text = self.arrCount[indexPath.row];
            cell.lblTime.text = self.arrTime[indexPath.row];
            let tot = tableView.numberOfRowsInSection(indexPath.section)
            if(indexPath.row == tot - 1){
                cell.lblSep.hidden = true
            }else{
                cell.lblSep.hidden = false
            }
            
            cell.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor .clearColor();
            backgroundView.alpha = 0.5
            cell.selectedBackgroundView = backgroundView

            return cell
 
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clearColor()
        appDelegate.selectIndex = indexPath.row
        self.performSegueWithIdentifier("segueMyProfile", sender: self)
    }
    @IBAction func btnScanAction(sender: UIButton) {
        appDelegate.customTabBarVC.selectView(1)
    }
    
        
    @IBAction func btnViewReportAction(sender: UIButton) {
        appDelegate.isDirectReport = false
        appDelegate.customTabBarVC.selectView(2)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
