//
//  SettingsVC.swift
//  CarDealer
//
//  Created by MAPPS MAC on 21/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit
enum EventTypeSett: Int {
    case action1 = 0
}
typealias EventCompletionHandlerSett = (obj:AnyObject?,EventTypeSett:Int ) -> Void
class SettingsVC: UIViewController {
     @IBOutlet weak var lblTitle: UILabel!
    var eventCompletionHandlerSett :EventCompletionHandlerSett!
    func setEventOnCompletion(handler:EventCompletionHandlerSett){
        eventCompletionHandlerSett = handler
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
