//
//  RootVC.swift
//  CarDealer
//
//  Created by MAPPS MAC on 21/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit

class RootVC: UIViewController {
    
    var homeVC : HomeVC!
    var scannerVC:ScannerVC!
    var reportingVC:ReportingVC!
    var settingsVC: SettingsVC!
    var appDelegate:AppDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
         self .setUpAllVc()
        // Do any additional setup after loading the view.
    }
    func setUpAllVc(){
        self.homeVC = appDelegate.storyboard.instantiateViewControllerWithIdentifier("HomeVC") as! HomeVC
        self.scannerVC = appDelegate.storyboard.instantiateViewControllerWithIdentifier("ScannerVC") as! ScannerVC
        self.reportingVC = appDelegate.storyboard.instantiateViewControllerWithIdentifier("ReportingVC") as! ReportingVC
        self.settingsVC = appDelegate.storyboard.instantiateViewControllerWithIdentifier("SettingsVC")as! SettingsVC
        var frm : CGRect = self.homeVC.view.frame
        frm.size.height = frm.size.height - 62;
        self.homeVC.view.frame = frm
        self.scannerVC.view.frame = frm
        self.reportingVC.view.frame = frm
        self.settingsVC.view.frame = frm
        
        self.homeVC .setEventOnCompletion { (obj, EventTypeH) -> Void in
            if(EventTypeH==0){
                self.performSegueWithIdentifier("segueEventDetails", sender: self)
            }
        }
        
        self.scannerVC.setEventOnCompletion { (obj, EventTypeS) -> Void in
            if(EventTypeS==0){
                
            }
        }
        
        self.reportingVC.setEventOnCompletion { (obj, EventTypeR) -> Void in
            if(EventTypeR==0){
                
            }
        }
        
        self.settingsVC.setEventOnCompletion { (obj, EventTypeSett) -> Void in
            if(EventTypeSett==0){
                
            }
        }
        
        self.view .addSubview(self.homeVC.view)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
