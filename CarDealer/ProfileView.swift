//
//  ProfileView.swift
//  CarDealer
//
//  Created by Aditi on 13/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit
class ProfileView: UIViewController {
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblGlobalTitle: UILabel!
    @IBAction func btnSearchAction(sender: UIButton) {
        self.btnSearch.hidden = true
        self.lblGlobalTitle.hidden = true
        self.viewSearch.hidden = false
        self.txtSearch .becomeFirstResponder()
    }
    
    @IBAction func btnCancelAction(sender: UIButton) {
        self.btnSearch.hidden = false
        self.lblGlobalTitle.hidden = false
        self.viewSearch.hidden = true
        self.txtSearch .resignFirstResponder()
        
    }
    @IBOutlet weak var viewSearch: UIView!

    
    
     @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    @IBAction func btnCallAction(sender: UIButton) {
        
        let phone = "+18889500700";
        
        
        if let url = NSURL(string: "tel://\(phone)") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
  
    @IBOutlet weak var lblSepActivity: UILabel!
    @IBOutlet weak var lblSepProfile: UILabel!
    @IBOutlet weak var viewMyProfile: UIView!
    @IBOutlet weak var viewMyActivity: UIView!
    
    @IBOutlet weak var lblNmae: UILabel!
    
    var appDelegate : AppDelegate!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    @IBAction func btnBackAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
        var arrSecProfile1Key = ["Address","City","State","Zip","Telephone","Email"]
     var arrSecProfile1Val = ["121 Graham Ave #4G","Brooklyn","NY","10011","1-917-555-0732","tej@gmail.com"]
    
    var arrSecProfile2Key = ["Company","Job Title","Estimated Salary","Make","Model","Trade-in Value"]
    var arrSecProfile2Val = ["Fresh Sporting Goods","Regional Manager","$80-90K","Hyundai","i20 Magna","$6,000"]
    
    var arrSecProfile3Key = ["Notes"]
    var arrSecProfile3Val = [""]
    
      var isMyProfile : Bool!
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        self.txtSearch.resignFirstResponder()
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtSearch.autocorrectionType = .No
        
        
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if(appDelegate.selectIndex==0 || appDelegate.selectIndex==3){
            var image : UIImage = UIImage(named:"profile-image-female.png")!
            self.imgProfile.image = image
           
            
            
            
        }else{
            var image : UIImage = UIImage(named:"profile-image.png")!
            self.imgProfile.image = image
            
        }
        self.lblNmae.text = appDelegate.arrName[appDelegate.selectIndex]
        
        let nib = UINib(nibName: "ProfileCell", bundle: nil)
        self.tblView.registerNib(nib, forCellReuseIdentifier: "CellP")
        
        let nib1 = UINib(nibName: "NoteCell", bundle: nil)
        self.tblView.registerNib(nib1, forCellReuseIdentifier: "CellN")
        
        let nib2 = UINib(nibName: "ActivityCell", bundle: nil)
        self.tblView.registerNib(nib2, forCellReuseIdentifier: "CellA")
        
        
        
        let nib3 = UINib(nibName: "ActivityCell2", bundle: nil)
        self.tblView.registerNib(nib3, forCellReuseIdentifier: "CellA2")


        
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("profileAction"))
        self.viewMyProfile.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: Selector("activityAction"))
        self.viewMyActivity.addGestureRecognizer(tap1)
        
        self.isMyProfile = true
        self.lblSepActivity.hidden = true
        self.viewMyActivity.backgroundColor = UIColor(red: 54/255, green: 61/255, blue:69/255, alpha: 0.5)
        // Do any additional setup after loading the view.
    }
    func profileAction(){
        self.isMyProfile = true
        self.lblSepProfile.hidden = false
        self.lblSepActivity.hidden = true
        self.viewMyProfile.backgroundColor = UIColor.blackColor()
        self.viewMyActivity.backgroundColor = UIColor(red: 54/255, green: 61/255, blue:69/255, alpha: 0.5)
        self.tblView .reloadData()
    }
    func activityAction(){
        self.isMyProfile = false
        self.lblSepProfile.hidden = true
        self.lblSepActivity.hidden = false
        self.viewMyProfile.backgroundColor = UIColor(red: 54/255, green: 61/255, blue:69/255, alpha: 0.5)
        self.viewMyActivity.backgroundColor =  UIColor.blackColor()
        self.tblView .reloadData()

    }
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(self.isMyProfile==true){
             return 3
        }else{
             return 1
        }
       
        
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(self.isMyProfile==true){
            return 10.0
        }else{
            return 0.0
        }
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 10.0)) //set these values as necessary
        returnedView.backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0.5)
        //returnedView.backgroundColor = UIColor.clearColor()
        let label = UILabel(frame: CGRectMake(10, 9, self.view.frame.size.width - 20, 1))
        label.backgroundColor = UIColor(red: 29/255, green: 36/255, blue: 44/255, alpha: 1.0)
        returnedView.addSubview(label)
        return returnedView
    }
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        if(self.isMyProfile == true){
        if(indexPath.section==2){
            return 127
        }else{
             return 30
        }
        }else{
            if(indexPath.row==0){
                return 200
            }else{
                return 218
            }
        }
        //222
    }
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if(self.isMyProfile == true){
        if(section==0){
            return self.arrSecProfile1Key.count
        }else if(section==1){
             return self.arrSecProfile2Key.count
        }else{
             return self.arrSecProfile3Key.count
        }
        }else{
            return 4
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(self.isMyProfile==true){
        if(indexPath.section==0){
            let cell:ProfileCell = tableView.dequeueReusableCellWithIdentifier("CellP") as! ProfileCell
            cell.lblleft.text = self.arrSecProfile1Key[indexPath.row];
            cell.lblRight.text = self.arrSecProfile1Val[indexPath.row];
            if(indexPath.row==0){
                cell.btnEdit.hidden = false
            }else{
                 cell.btnEdit.hidden = true
            }
            
            cell.backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0.5)
           let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor .clearColor();
             backgroundView.alpha = 0.5
            cell.selectedBackgroundView = backgroundView
          return cell

        }else if(indexPath.section==1){
            let cell:ProfileCell = tableView.dequeueReusableCellWithIdentifier("CellP") as! ProfileCell
            cell.lblleft.text = self.arrSecProfile2Key[indexPath.row];
            cell.lblRight.text = self.arrSecProfile2Val[indexPath.row];
            if(indexPath.row==0){
                cell.btnEdit.hidden = false
            }else{
                cell.btnEdit.hidden = true
            }
            
            cell.backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0.5)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor .clearColor();
            backgroundView.alpha = 0.5
            cell.selectedBackgroundView = backgroundView
            return cell

        }else{
            let cell:NoteCell = tableView.dequeueReusableCellWithIdentifier("CellN") as! NoteCell
             cell.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
//            let backgroundView = UIView()
//            backgroundView.backgroundColor = UIColor.clearColor()
//            cell.selectedBackgroundView = backgroundView
            return cell

        }
        }else{
            if(indexPath.row==0){
            let cell:ActivityCell2 = tableView.dequeueReusableCellWithIdentifier("CellA2") as! ActivityCell2
            
            cell.backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0.5)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor .clearColor();
            backgroundView.alpha = 0.5
            cell.selectionStyle = .None
            return cell
            }else{
                let cell:ActivityCell = tableView.dequeueReusableCellWithIdentifier("CellA") as! ActivityCell
                
                cell.backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0.5)
                let backgroundView = UIView()
                backgroundView.backgroundColor = UIColor .clearColor();
                backgroundView.alpha = 0.5
                cell.selectionStyle = .None
                return cell

            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
