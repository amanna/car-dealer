//
//  HomeVC.swift
//  CarDealer
//
//  Created by MAPPS MAC on 10/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit
enum EventTypeH: Int {
    case kselect = 0
}
typealias EventCompletionHandlerH = (obj:AnyObject?,EventTypeH:Int ) -> Void
class HomeVC: UIViewController {

    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblGlobalTitle: UILabel!
    @IBAction func btnSearchAction(sender: UIButton) {
        self.btnSearch.hidden = true
        self.lblGlobalTitle.hidden = true
        self.viewSearch.hidden = false
        self.txtSearch .becomeFirstResponder()
    }
    
    @IBAction func btnCancelAction(sender: UIButton) {
        self.btnSearch.hidden = false
        self.lblGlobalTitle.hidden = false
        self.viewSearch.hidden = true
        self.txtSearch .resignFirstResponder()
        
    }
    @IBOutlet weak var viewSearch: UIView!

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewSchedule: UIView!
    var appDelegate:AppDelegate!
   
    var arrEvent = ["Dealership Event 1","Dealership Event 2","Dealership Event 3","Dealership Event 4"]
    var arrEventDt = ["10/11/15","TODAY","11/8/15","11/12/15"]
    
    var arrAttendee = ["34","50","56","40"]
    var arrRsvp = ["65","57","78","56"]
    var eventCompletionHandlerH :EventCompletionHandlerH!
    func setEventOnCompletion(handler:EventCompletionHandlerH){
        eventCompletionHandlerH = handler
    }
    func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
         self.txtSearch.resignFirstResponder()
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtSearch.autocorrectionType = .No
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let nib = UINib(nibName: "EventCell", bundle: nil)
        self.tblView.registerNib(nib, forCellReuseIdentifier: "Cell")
         self.tblView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.viewSchedule.backgroundColor = UIColor .blackColor();
        self.viewSchedule.alpha = 0.5
       self.viewSchedule.layer.cornerRadius = 5.0

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEvent.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:EventCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! EventCell
        cell.lblTitle.text = self.arrEvent[indexPath.row];
        cell.lblDate.text = self.arrEventDt[indexPath.row];
        if(indexPath.row==0){
            cell.lblDate.textColor = UIColor(red: 67/255, green: 107/255, blue: 170/255, alpha: 1.0)
        }else{
            cell.lblDate.textColor = UIColor(red: 70/255, green: 244/255, blue: 28/255, alpha: 1.0)
        }
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = backgroundView
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //selectaction
       // eventCompletionHandlerH(obj: nil,EventTypeH:0)
        
        appDelegate.selectEventIndex = indexPath.row
        var selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clearColor()
        self.performSegueWithIdentifier("segueEventDetails", sender: self)
    }
   
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


