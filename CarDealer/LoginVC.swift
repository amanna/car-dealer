//
//  LoginVC.swift
//  CarDealer
//
//  Created by Aditi on 28/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    var appDelegate:AppDelegate!
    @IBOutlet weak var txtUser: UITextField!
    
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewSchedule: UIView!

    @IBAction func btnLoginAction(sender: UIButton) {
        appDelegate.setUpTabBar()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField==self.txtUser){
            self.txtUser.resignFirstResponder()
            self.txtPass.becomeFirstResponder()
        }else{
            self.txtPass.resignFirstResponder()
        }
        return true;
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        var addStatusBar = UIView()
        addStatusBar.frame = CGRectMake(0, 0, self.view.frame.size.width, 20);
        addStatusBar.backgroundColor = UIColor.blackColor()
        self.view .addSubview(addStatusBar)
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.txtUser.layer.cornerRadius = 8.0
        self.txtUser.layer.masksToBounds = true
        self.txtUser.layer.borderColor = UIColor( red: 46/255, green: 118/255, blue:183/255, alpha: 1.0 ).CGColor
        self.txtUser.layer.borderWidth = 1.0
        
        
        self.txtPass.layer.cornerRadius = 8.0
        self.txtPass.layer.masksToBounds = true
        self.txtPass.layer.borderColor = UIColor( red: 46/255, green: 118/255, blue:183/255, alpha: 1.0 ).CGColor
        self.txtPass.layer.borderWidth = 1.0
        
        
        self.txtUser.autocorrectionType = UITextAutocorrectionType.No
        self.txtPass.autocorrectionType = UITextAutocorrectionType.No
        
      self.txtUser.setValue(UIColor.whiteColor(), forKeyPath: "_placeholderLabel.textColor")
      self.txtPass.setValue(UIColor.whiteColor(), forKeyPath: "_placeholderLabel.textColor")
      
      let paddingView = UIView(frame: CGRectMake(0, 0, 15, self.txtUser.frame.height))
      self.txtUser.leftView = paddingView
      self.txtUser.leftViewMode = UITextFieldViewMode.Always
        
        let paddingView1 = UIView(frame: CGRectMake(0, 0, 15, self.txtPass.frame.height))
        self.txtPass.leftView = paddingView1
        self.txtPass.leftViewMode = UITextFieldViewMode.Always
//        
//        self.txtPass.leftView = paddingView
//        self.txtPass.leftViewMode = UITextFieldViewMode.Always
//46
      //  118
     //   183
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
